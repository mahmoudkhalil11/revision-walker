namespace RevisionWalker
{
    public class Application : Gtk.Application
    {
        public Application()
		{
			Object(application_id : "org.gnome.revision-walker",
				flags: ApplicationFlags.FLAGS_NONE);
		}

		protected override void activate()
		{
		    Ggit.init();

		    var window = new RevisionWalker.Window(this);
		    window.set_title("Walkers");
		    window.set_default_size(750, 500);
            window.build_ui();

		    window.show_all();
		}
    }
}
