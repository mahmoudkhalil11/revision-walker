namespace RevisionWalker
{
    [GtkTemplate (ui = "/org/gnome/revision-walker/ui/revision-walker-window.ui")]
    public class Window : Gtk.ApplicationWindow
    {
        [GtkChild]
        Gtk.Stack d_main_stack;

        [GtkChild]
        Gtk.RadioButton d_multiple_walkers_radio_button;

        [GtkChild]
        Gtk.RadioButton d_single_walker_radio_button;

        [GtkChild]
        Gtk.Button d_start_button;

        [GtkChild]
        Gtk.TreeView d_tree_view_0;

        [GtkChild]
        Gtk.TreeView d_tree_view_1;

        Gtk.ListStore d_first_list_store;
        Gtk.ListStore d_second_list_store;

        private Ggit.Repository? repo;

        private signal void two_walkers_finished();
        private signal void one_walker_finsihed_first_list();
        private signal void one_walker_finsihed_second_list();

        public Window(Application app)
        {
            Object(application : app);
        }

        public void build_ui()
        {
            d_first_list_store = new Gtk.ListStore(1, GLib.Type.STRING);
            d_second_list_store = new Gtk.ListStore(1, GLib.Type.STRING);
            try
            {
                repo = Ggit.Repository.open(File.new_for_path("/home/mahmoud/Projects/gtk/.git/"));
            }
            catch(Error err)
            {
                stderr.printf("Could load repository: %s\n", err.message);
            }

            if(repo == null)
            {
                return;
            }

            this.two_walkers_finished.connect(() => {
                d_tree_view_0.set_model(d_first_list_store);
                d_tree_view_1.set_model(d_second_list_store);

                Gtk.CellRenderer first_renderer = new Gtk.CellRendererText();
                Gtk.CellRenderer second_renderer = new Gtk.CellRendererText();
                d_tree_view_0.insert_column_with_attributes(-1, "Commits", first_renderer, "text", 0);
                d_tree_view_1.insert_column_with_attributes(-1, "Commits", second_renderer, "text", 0);
            });

            this.one_walker_finsihed_first_list.connect(() => {
                d_tree_view_0.set_model(d_first_list_store);

                Gtk.CellRenderer renderer = new Gtk.CellRendererText();

                d_tree_view_0.insert_column_with_attributes(-1, "Commits", renderer, "text", 0);
            });

            this.one_walker_finsihed_second_list.connect(() => {
                d_tree_view_1.set_model(d_second_list_store);

                Gtk.CellRenderer renderer = new Gtk.CellRendererText();

                d_tree_view_1.insert_column_with_attributes(-1, "Commits", renderer, "text", 0);
            });

            d_start_button.clicked.connect(() => {
                d_main_stack.set_visible_child_name("page1");

                if(d_single_walker_radio_button.get_active())
                {
                    new GLib.Thread<void*>("one walker", read_one_walker);
                }
                else
                {
                    new GLib.Thread<void*>("two walkers", read_two_walkers);
                }
            });
        }

        private void* read_two_walkers()
        {
            Ggit.RevisionWalker? first_walker = null;
            Ggit.RevisionWalker? second_walker = null;

            try
            {
                first_walker = new Ggit.RevisionWalker(repo);
                second_walker = new Ggit.RevisionWalker(repo);

                first_walker.reset();
                second_walker.reset();

                first_walker.set_sort_mode(Ggit.SortMode.REVERSE);
                second_walker.set_sort_mode(Ggit.SortMode.REVERSE);

                first_walker.push_head();
                second_walker.push_head();

                var first_oid = first_walker.next();
                var second_oid = second_walker.next();

                while(first_oid != null && second_oid != null)
                {
                    var first_commit = repo.lookup_commit(first_oid);
                    var second_commit = repo.lookup_commit(second_oid);

                    debug("Found Commit: %s", first_commit.get_subject());
                    debug("Found Commit: %s", second_commit.get_subject());

                    Gtk.TreeIter first_iter, second_iter;
                    d_first_list_store.append(out first_iter);
                    d_second_list_store.append(out second_iter);

                    d_first_list_store.set(first_iter, 0, first_commit.get_subject());
                    d_second_list_store.set(second_iter, 0, second_commit.get_subject());

                    first_oid = first_walker.next();
                    second_oid = second_walker.next();
                }
            }
            catch(Error err)
            {
                debug(err.message);
            }
            two_walkers_finished();
            return null;
        }

        private void* read_one_walker()
        {
            Ggit.RevisionWalker? first_walker = null;

            try
            {
                first_walker = new Ggit.RevisionWalker(repo);

                first_walker.reset();

                first_walker.set_sort_mode(Ggit.SortMode.REVERSE);

                first_walker.push_head();

                var first_oid = first_walker.next();

                while(first_oid != null)
                {
                    var first_commit = repo.lookup_commit(first_oid);

                    debug("Found Commit: %s", first_commit.get_subject());

                    Gtk.TreeIter first_iter, second_iter;
                    d_first_list_store.append(out first_iter);

                    d_first_list_store.set(first_iter, 0, first_commit.get_subject());

                    first_oid = first_walker.next();
                }
            }
            catch(Error err)
            {
                debug(err.message);
            }
            one_walker_finsihed_first_list();

            try
            {
                first_walker.reset();

                first_walker.set_sort_mode(Ggit.SortMode.REVERSE);

                first_walker.push_head();

                var first_oid = first_walker.next();

                while(first_oid != null)
                {
                    var first_commit = repo.lookup_commit(first_oid);

                    debug("Found Commit: %s", first_commit.get_subject());

                    Gtk.TreeIter first_iter, second_iter;
                    d_second_list_store.append(out first_iter);

                    d_second_list_store.set(first_iter, 0, first_commit.get_subject());

                    first_oid = first_walker.next();
                }
            }
            catch(Error err)
            {
                debug(err.message);
            }
            one_walker_finsihed_second_list();

            return null;
        }
    }
}
