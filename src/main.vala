namespace RevisionWalker
{
    public class Main
    {
        public static int main(string[] args)
        {
            return new RevisionWalker.Application().run(args);
        }
    }
}
